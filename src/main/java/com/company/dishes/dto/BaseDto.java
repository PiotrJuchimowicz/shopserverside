package com.company.dishes.dto;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class BaseDto {
    private Long id;
}
